// test.js


const { assert } = require('chai');
const { newUser } = require('../index.js');

describe('Test newUser Object', () => {

	it('Assert newUser type is object', () => {

		assert.equal(typeof(newUser),'object')

	});

	it('Assert newUser.email type is not undefined', () => {

		assert.notEqual(typeof(newUser.email), 'undefined')

	});

	it('Assert newUser.email is type string', () => {

		assert.equal(typeof(newUser.email),'string')

	});

	it('Assert newUser.password is type string', () => {

		assert.equal(typeof(newUser.password),'string')

	});	

	it('Assert that the number of characters in newUser.password is at least 16', () => {

		assert.isAtLeast(newUser.password.length,16)

	});

	//ACTIVITY

/* 1 */
	it('Assert newUser.firstName type is string', () => {

		assert.equal(typeof(newUser.firstName),'string')

	});

/* 2 */
	it('Assert newUser.lastName type is string', () => {

		assert.equal(typeof(newUser.lastName),'string')

	});

/* 3 */
	it('Assert newUser.firstName type is not undefined', () => {

		assert.notEqual(typeof(newUser.firstName), 'undefined')

	});

/* 4 */
	it('Assert newUser.lastName type is not undefined', () => {

		assert.notEqual(typeof(newUser.lastName), 'undefined')

	});

/* 5 */
	it('Assert newUser.age is at least 18', () => {

		assert.isAtLeast(newUser.age,18)

	});

/* 6 */
	it('Assert newUser.age type is number', () => {

		assert.equal(typeof(newUser.age),'number')

	});

/* 7 */
	it('Assert newUser.contactNumber type is string', () => {

		assert.equal(typeof(newUser.contactNumber),'string')

	});

/* 8 */
	it('Assert newUser.contactNumber is at least 11 characters', () => {

		assert.isAtLeast(newUser.contactNumber.length,11)

	});

/* 9 */
	it('Assert newUser.batchNumber type is number', () => {

		assert.equal(typeof(newUser.batchNumber),'number')

	});

/* 10 */
	it('Assert newUser.batchNumber type is not undefined', () => {

		assert.notEqual(typeof(newUser.batchNumber), 'undefined')

	});



});


/*
	- describe gives structure to your test suite
	- it() is the unit tests

*/